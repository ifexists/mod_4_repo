// This functions page is responsible for methods of
// the Game_Lobby.cpp file. Without this file in the same direction as 
// Game_Lobby.cpp - the application will not compile
// function declarations
#include <iostream>
#include <string>

//Game Menu Functions for game-menu.cc
using namespace std;

// Tell us something about yourself
// Name /.- 
// General Location - 

// Set up your global variables here


// Define your functions here

void PressN2Continue() {
      cout << "Press 'n' then <Enter> to continue....\n";
      
    while (1) {
        if ('n' == getchar())
            break;
    }
    
}
  
void hello() {

    cout << "Hello Player!\n";
    PressN2Continue();
    return;
}

void version() {

    cout << "Game virsion: 1.01\n";
    PressN2Continue();
    return;
}


// This is where game lobby functions begin
class Player
{
public:
    Player(const string& name = "");
    string GetName() const;
    Player* GetNext() const;
    void SetNext(Player* next);

private:
    string m_Name;
    Player* m_pNext;  //Pointer to next player in list
};

Player::Player(const string& name):
    m_Name(name),
    m_pNext(0)
{}

string Player::GetName() const
{
    return m_Name;
}

Player* Player::GetNext() const
{
    return m_pNext;
}

void Player::SetNext(Player* next)
{
    m_pNext = next;
}

class Lobby
{
    friend ostream& operator<<(ostream& os, const Lobby& aLobby);

public:
    Lobby();
    ~Lobby();
    void AddPlayer();
    void RemovePlayer();
    void Clear();

private:
    Player* m_pHead;
};

Lobby::Lobby():
    m_pHead(0)
{}


void Lobby::AddPlayer()
{
    //create a new player node
    cout << "Please enter the name of the new player: ";
    string name;
    cin >> name;
    Player* pNewPlayer = new Player(name);

    //if list is empty, make head of list this new player
    if (m_pHead == 0)
    {
        m_pHead = pNewPlayer;
    }
    //otherwise find the end of the list and add the player there
    else
    {
        Player* pIter = m_pHead;
        while (pIter->GetNext() != 0)
        {
            pIter = pIter->GetNext();
        }
        pIter->SetNext(pNewPlayer);
    }
}

void Lobby::RemovePlayer()
{
    cout << "Which player do you want to remove: ";
    
    string name;
    cin >> name;

    //if list is empty, make head of list this new player
    if (m_pHead == 0)
    {
        cout << "No one to remove";
    }
    
    else {
        Player* remover = m_pHead;
        cout << remover->GetName() << " will be removed..." << endl;
        string sean = remover->GetName();
        
        m_pHead = 0;
    
    }
   
}

Lobby::~Lobby()
{
    Clear();
}


void Lobby::Clear()
{
    m_pHead = 0;

}

ostream& operator<<(ostream& os, const Lobby& aLobby)
{
    Player* pIter = aLobby.m_pHead;

    os << "\nHere's who's in the game lobby:\n";
    if (pIter == 0)
    {
        os << "The lobby is empty.\n";
    }
    else
    {
        while (pIter != 0)
        {
            os << pIter->GetName() << endl;
	        pIter = pIter->GetNext();
        }
    }

    return os;
}

// Begin classes and functions for the critter game

class Critter
{
public:
    Critter(int hunger = 0, int boredom = 0);
    void Talk();
    void Play();
    void Eat();


private:
    int m_Hunger;
    int m_Boredom;

    int GetMood() const;
    void PassTime(int time = 1);

};

Critter::Critter(int hunger, int boredom):
    m_Hunger(hunger),
    m_Boredom(boredom)
{}

inline int Critter::GetMood() const
{
    return (m_Hunger + m_Boredom);
}

void Critter::PassTime(int time)
{
    m_Hunger += time;
    m_Boredom += time;
}

void Critter::Eat()
{
    m_Hunger = m_Hunger - 1;
    cout << "\nGood job, you fed your critter!" << endl;
}

void Critter::Play()
{
    m_Boredom = m_Boredom - 1;
    cout << "\nGood job, you played with your critter!" << endl;
}

void Critter::Talk()
{
    cout << "I'm a critter and I feel ";

    int mood = GetMood();
    if (mood > 15)
	{
        cout << "mad.\n";
	}
    else if (mood > 10)
	{
        cout << "frustrated.\n";
	}
    else if (mood > 5)
	{
        cout << "okay.\n";
	}
    else
	{
        cout << "happy.\n";
	}

    PassTime();
}


