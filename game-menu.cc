#include <iostream>
#include <limits>
#include "functions.h"
#include <fstream> 
#include <stdlib.h> 
#include <stdio.h> 
#include <string.h> 

using namespace std;
ifstream inFile;


int main() {
    
    int choice;
    
    do {
        cout << "\nGAME MENU\n";
        cout << "0 - Exit the program.\n";
        cout << "1 - Greetings\n";
        cout << "2 - Version\n";
        cout << "3 - Critter Game\n";
        cout << "4 - Game Lobby\n";
        cout << "5 - Hang Man\n";
        cout << "6 - Black Jack\n";
        cout << endl << "Enter choice: ";
        cin >> choice;

        switch (choice) {
            case 0: cout << "Good-bye.\n"; break;
            case 1: hello(); break;
            case 2: version(); break;
            case 3: system("./critter");
            case 4: system("./lobby");
            case 5: system("./hang-man");
            case 6: system("./blackjack");
            default: cout << "That was not a valid choice.\n";
        }
    } while (choice != 0);

    return 0;
}
